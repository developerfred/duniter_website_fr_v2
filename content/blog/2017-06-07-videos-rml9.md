+++
aliases = [ "videos-rml9",]
date = 2017-06-07
title = "RML9 : vidéos des journées techniques"

[extra]
thumbnail = "/PELICAN/images/play.svg"

[taxonomies]
authors = [ "cgeek",]
tags = [ "site",]
category = [ "Évènements",]
+++

# RML9 : vidéos des journées techniques

## Les vidéos des deux journées techniques sont désormais disponibles !

Revivez à votre rythme les tutoriaux pour développer des plugins Sakia, Cesium et Duniter.

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL0UDqLtXevvH2GRaD5-HUPWExVxY_MuwK" frameborder="0" allowfullscreen></iframe>

Vous y trouverez également les interventions de : 

* Moul à propos de l'utilisation de Silkaj et l'API HTTP de Duniter
* Tortue à propos des [paperwallet](https://duniter.tednet.fr/paperwallet/)
* gpsqueeek pour l'installation de Duniter sur une brique Internet
* Eloïs qui présente son nœud spécialisé [duniter-currency-monit](https://github.com/duniter/duniter-currency-monit)
* kimamila qui présente les nouveautés de Cesium

![Paper Wallet](/PELICAN/images/rml9/paperwallet.png)

## RML10 à Montpellier

Rendez-vous pour les prochaines interventions techniques des [**RML10 du 16 au 19 NOVEMBRE 2017 à MONTPELLIER**](https://rml10.duniter.org/) !

