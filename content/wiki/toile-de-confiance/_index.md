+++
title = "Toile de confiance"
weight = 4
sort_by = "weight"

aliases = ["fr/toile-de-confiance"]
+++

# La Toile de Confiance

<iframe width="560" height="315" src="https://www.youtube.com/embed/xCsjIdeHPJc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*   [Fonctionnement de la Toile de confiance](@/wiki/toile-de-confiance/introduction-a-la-toile-de-confiance.md)
*   [La Toile de confiance en détail](@/wiki/toile-de-confiance/la-toile-de-confiance-en-detail.md)
*   [Étude de la WoT sur le forum Duniter](https://forum.duniter.org/t/etude-de-la-wot/977)
*   [Comment certifier de nouveaux membres](@/FAQ/certifier-de-nouveaux-membres.md)
*   [Questions fréquentes sur la Toile de confiance](@/FAQ/faq-tdc.md)

