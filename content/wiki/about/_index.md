+++
title = "À propos"
date = 2017-03-27
updated = 2020-07-26
weight = 6

[taxonomies]
authors = [ "cgeek",]
+++

# À propos

## Contenus

L'intégralité de ce site est sous licence [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) sauf mention contraire explicite.

## Code source

Ce site a été réalisé en [Zola](https://www.getzola.org/) avec [w3.css](https://www.w3schools.com/w3css/) et [Forkawesome](https://forkaweso.me/Fork-Awesome/).

Le code source du site est par ailleurs intégralement disponible sur le dépôt [git.duniter.org/websites/website_fr](https://git.duniter.org/websites/website_fr) avec les instructions permettant de le reproduire.

## Images utilisées sur ce site

Certains logos ou images sont la production d'autres personnes, vous trouverez leur origine sur la page [Crédits](@/wiki/about/credits.md).

## Qui sommes-nous ?

Plus de détails sur nous dans la page [Qui sommes-nous ?](@/wiki/about/qui-sommes-nous.md)

## Nous contacter

Pour nous contacter, rendez-vous sur [la page de contact](@/wiki/about/contact.md)
