+++
title = "Contributeurs"
date = 2020-07-29
template = "custom/equipe.html"
page_template = "authors/page.html"

[extra]
# contrôle la liste des contributeurs (nom de fichier)
authors = [
    "cgeek",
    "elois",
    "kimamila",
    "inso",
    "Moul",
    "Galuel",
    "1000i100",
    "vit",
    "vincentux",
    "Gérard",
    "Paidge",
    "HugoTrentesaux",
    ]
+++

Nous listons ici les personnes ayant apporté une contribution technique au projet.