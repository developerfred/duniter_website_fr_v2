+++
title = "Hugo Trentesaux"
description = "Mainteneur du site duniter"

[extra]
avatar = "HugoTrentesaux.png"
website = "https://trentesaux.fr/"
forum_duniter = "HugoTrentesaux"
forum_ml = "Hugo-Trentesaux"
g1_pubkey = "55oM6F9ZE2MGi642GGjhCzHhdDdWwU6KchTjPzW7g3bp"
phone = "+33 6 49 88 18 21"
email = "hugo@trentesaux.fr"
+++

Hugo a apporté de petites contributions techniques à Dunitrust et Duniterpy. Il a écrit le template du site Duniter et maintient le site.