+++
title = "Cédric Moreau"
description = "Cédric Moreau est le développeur original du logiciel Duniter"

[extra]
avatar = "cgeek.png"
website = "https://blog.cgeek.fr/"
forum_duniter = "cgeek"
forum_ml = "cgeek"
+++

Cédric Moreau est le développeur original du logiciel Duniter et auteur principal du protocole.