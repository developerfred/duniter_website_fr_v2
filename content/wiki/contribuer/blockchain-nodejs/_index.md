+++
title = "Contribuer à Duniter"
date = 2017-04-08
aliases = [ "fr/blockchain-nodejs",]
weight = 9

[taxonomies]
authors = [ "cgeek",]
+++

# Contribuer à Duniter

## Concepts généraux

* [Preuve de travail](@/wiki/contribuer/blockchain-nodejs/preuve-de-travail.md)
* [Architecture](@/wiki/contribuer/blockchain-nodejs/architecture-duniter.md)
* [Réflexions techniques autour de Duniter et de sa philosophie \[archives\]](@/wiki/contribuer/blockchain-nodejs/archives.md)

## Implémentation

* [Tutoriel de développement](@/wiki/contribuer/blockchain-nodejs/tutoriel-dev/_index.md)
* [Fonctionnement des branches du dépot git duniter](@/wiki/contribuer/blockchain-nodejs/git-branches.md)
* [Fuites mémoire](@/wiki/contribuer/blockchain-nodejs/fuites-memoire.md)
* [Les modules C/C++](@/wiki/contribuer/blockchain-nodejs/les-modules-c-cpp.md)
* [Guide du validateur](https://git.duniter.org/nodes/typescript/duniter/blob/dev/doc/validator-guide.md)
* [Livraisons](@/wiki/contribuer/blockchain-nodejs/livraisons.md)
* [Livraisons (ancien)](@/wiki/contribuer/blockchain-nodejs/livraisons-old.md)
* [Du Rust dans NodeJs graçe a Neon](@/wiki/contribuer/blockchain-nodejs/binding-node-rust.md)


