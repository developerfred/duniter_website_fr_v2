+++
aliases = [ "tutoriel-dev",]
date = 2017-10-31
weight = 1
title = "Tutoriel de développement"

[taxonomies]
authors = [ "cgeek",]
+++

# Tutoriel de développement

{% note(type="warning", display="icon") %} Tutoriel en cours de rédaction ! (06/11/2017){% end %}

Vous souhaitez manipuler le code de Duniter, comprendre ses rouages par curiosité ou même contribuer à son développement ?

Ce tutoriel tentera de vous y aider !

L'apprentissage est découpé en plusieurs chapitres progressifs, chacun d'entre eux visant à maîtriser un aspect spécifique du logiciel et de ses outils.

## En cas de difficultés

En cas de difficultés, rendez-vous sur [le forum technique Duniter](https://forum.duniter.org). 

Posez votre question, décrivez votre problème et des contributeurs tenteront de vous aider.

Place aux chapitres !

## Sommaire

1. [Présentation générale et philosophie du projet](@/wiki/contribuer/blockchain-nodejs/tutoriel-dev/chapitre-1-presentation.md)
2. [Installation des outils](@/wiki/contribuer/blockchain-nodejs/tutoriel-dev/chapitre-2-outils.md)
3. [Récupération du code source](@/wiki/contribuer/blockchain-nodejs/tutoriel-dev/chapitre-3-source.md)
4. [Démarrage](@/wiki/contribuer/blockchain-nodejs/tutoriel-dev/chapitre-4-demarrage.md)
5. [Architecture de Duniter](@/wiki/contribuer/blockchain-nodejs/tutoriel-dev/chapitre-5-architecture.md)
6. [Éléments de code](@/wiki/contribuer/blockchain-nodejs/tutoriel-dev/chapitre-6-code.md)
7. [La base de données](@/wiki/contribuer/blockchain-nodejs/tutoriel-dev/chapitre-7-bdd.md)
8. [Les addons C/C++](@/wiki/contribuer/blockchain-nodejs/tutoriel-dev/chapitre-8-addons.md) (en cours)

