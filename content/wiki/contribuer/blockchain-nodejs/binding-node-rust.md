+++
title = "Du Rust dans NodeJs graçe a Neon"
date = 2018-05-04
aliases = [ "fr/binding-node-rust",]
weight = 9

[taxonomies]
authors = [ "elois",]
+++

# Du Rust dans NodeJs graçe a Neon

# Binding Node Rust

Dans ce tutoriel nous allons voir comment intégrer du code [Rust](https://www.rust-lang.org) dans un module NodeJs graçe a [Neon](https://github.com/neon-bindings/neon).

Il existe plusieurs façon de faire du bingind Node-Rust, mais la seule qui permet de bénéficier des garanties du Rust dans le binding c'est Neon : 

- Neon permet a Rust de manipuler directement des types JS et de caster d’un type JS vers un type Rust et vice versa dans une macro try dont les exceptions sont captés par la VM Node !!!
- Les variables de type JS créer par le code Rust et fournies en sortie sont gérés par le GC de Node, donc le binding ne peut pas causer de fuite mémoire.

Les deux autrés méthodes sont passer par une 3ème language intermédiare (le C++) ou utiliser la FFI native de Rust. Mais les deux font perdre les garanties du Rust car dans las deux cas V8 manipule le binaire finale comme si c'était du C/C++. Neon est donc une révolution dans le sens ou les garanties du Rust sont conservées.


Vous aurez besoin des outils de développement pour Rust et pour NodeJS, les indications d'installation sont valable pour les systèmes gnu/linux basés sur debain ou dérivés :

## 1. Installation votre enviromment Rust

Suivez ce tutoriel : [Installer son environnement Rust](@/wiki/contribuer/blockchain-rust/installer-son-environnement-rust.md)

## 2. Installez NodeJS

Installez nvm : 

```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
```

Puis installez la version de NodeJs que vous souhaitez utiliser : 

```bash
nvm install VERSION
```

## 3. Installez Neon

Installez l'utilitaire neon-cli, c'est indispensable :

```bash
npm install -g neon-cli
```

## Votre Hello Word en Rust appelé par Node

(en cours de rédaction...)
