+++
title = "Améliorer le site"
date = 2017-03-31
aliases = [ "fr/ameliorer-le-site",]
weight = 9

[taxonomies]
authors = [ "cgeek",]
+++

# Améliorer le site

Ce site est comme un logiciel libre : tout son code source est disponible et modifiable [sur GitLab](https://git.duniter.org/websites/website_fr) .

Vous trouverez dans le pied de presque chacune des pages du site un bouton qui ressemble à celui-ci : 

<a href="#" class="btn btn-primary gitlab">Modifiez cette page sur GitLab</a>

En cliquant dessus, vous serez redirigé notre plateforme GitLab qui héberge le code source du site. *Vous pourrez donc le modifier*.

## Nécessite un compte GitHub

Toutefois après avoir cliqué sur ce bouton, vous noterez rapidement que GitLab requiert d'ouvrir un compte GitHub pour aller plus loin. Cela peut vous rebuter, mais sachez que vous n'aurez besoin de créer ce compte qu'une fois pour modifier toutes les pages de notre site (et même d'autres sites proposant cette fonctionnalité).

Ce fonctionnement **permet à tous** de corriger une faute de frappe, une mauvaise tournure de phrase, ou même de proposer de nouveaux contenus sans attendre !

## Est soumis à modération

Bien sûr, vous ne pouvez pas modifier le site sans contrôle de notre part. Mais sachez que grâce à GitHub, nous pouvons *très rapidement répondre* à votre proposition : il nous suffit de cliquer sur un bouton, et hop ! vos modifications apparaissent dans les secondes qui suivent.

## Comment créer une nouvelle page, un nouvel article

Rédiger un nouveau contenu est un peu plus difficile, puisqu'il faut créer un ou plusieurs nouveaux fichiers.

Nous détaillerons cela un peu plus tard. Mais si vous vous sentez déjà à l'aise avec GitHub, sachez qu'ajouter un article sur ce blog revient simplement à ajouter un fichier au format `YYYY-MM-DD-nom-article.md` dans le répertoire `content/` pour un article (par exemple `2017-04-28-article-du-28-avril`), ou un fichier `nom-de-la-page.md` dans le répertoire `content/pages/` ou l'un de ses sous-répertoires selon la catégorie visée, de rédiger son contenu en s'inspirant d'autres articles déjà existants pour finalement envoyer une *pull request* via le bouton dédié sur GitHub.

De nôtre côté, la validation est toujours un simple bouton.
