+++
title = "Documentation Duniter"
weight = 1
sort_by = "weight"

aliases = ["fr/miner-des-blocs"]
+++

TODO guider dans la documentation

## Comment installer un nœud Duniter

### Installer son nœud Duniter

*   [Installer son nœud Duniter](@/wiki/documentation/installer/index.md)
*   [Mettre à jour son nœud Duniter](@/wiki/documentation/mettre-a-jour.md)
*   [Mettre à jour Duniter sur YunoHost en ligne de commande](https://forum.duniter.org/t/full-https-support-for-duniter-package-for-yunohost/1892/18)

### Configurer son nœud Duniter

* [Configurer son nœud Duniter fraichement installé](@/wiki/documentation/configurer.md)
* [Avoir plusieurs pairs partageant une même clé](@/wiki/documentation/cles-partagees.md)
* [Ajouter/Retirer des interfaces spécifiques de pair](@/wiki/documentation/interfaces-specifiques-de-pair.md)

### Utiliser son nœud Duniter

*   [Lancer Duniter automatiquement au démarrage de la machine](@/wiki/documentation/lancement-au-boot.md)
*   [Duniter en ligne de Commande](@/wiki/documentation/commandes.md)
*   [Les modules (plugins)](@/wiki/documentation/modules.md)
*   [Liste des modules (plugins)](@/wiki/documentation/modules.md)

### Auto-héberger son nœud Duniter

*   [Duniter sur un VPN](https://forum.duniter.org/t/duniter-sur-un-vpn/2280/13)
*   [Duniter sur YunoHost derrière une box privatrice (type livebox)](https://forum.duniter.org/t/duniter-sur-yunohost-derriere-une-box-privatrice-type-livebox/2169)
*   [Duniter sur Raspberry Pi 3 derrière une Freebox v5 avec Yunohost](@/wiki/documentation/raspberry-pi-freebox-yunohost.md)

## Rémunération des forgerons

*   [Comment être rémunéré en calculant des blocs](@/wiki/documentation/remuneration-calcul-blocs.md)
