+++
title = "WS2P preferred and privileged nodes"

[extra]
EXTERNAL_CONTENT = "https://git.duniter.org/nodes/typescript/duniter/-/raw/dev/doc/use/ws2p_preferred_privileged.md"
auto_toc = true
+++